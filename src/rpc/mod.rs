mod xi_consumer;
mod xi_producer;

use std::process::{Command, Stdio};
use std::sync::{Arc, Mutex};
use std::thread;
use std::io::BufReader;

use rpc::xi_consumer::*;
use rpc::xi_producer::*;

use xi_rpc::*;
use serde_json::Value;

pub trait RpcEmitter {
    fn send_notification(&self, method: &str, params: &Value);
    fn send_request(&self, method: &str, params: &Value) -> Result<Value, Error>;
}

pub fn start_xi_core() -> XiClientProducer {
    // Start the daemon as a child.
    let xi_process = Command::new("xi-core")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::inherit())
        .spawn()
        .unwrap();

    // Retrieve the pipes.
    let stdin = xi_process.stdin.unwrap();
    let stdout = BufReader::new(xi_process.stdout.unwrap());

    // Create the client.
    let inner_client = RpcLoop::new(stdin);

    // Create a thread safe writer
    let peer = Arc::new(Mutex::new(inner_client.get_raw_peer().box_clone()));

    // Start the main loop.
    thread::spawn(move || {
        inner_client
            .mainloop(|| stdout, &mut XiClientConsumer {})
            .unwrap()
    });

    XiClientProducer::new(peer)
}
