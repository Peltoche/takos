use std::sync::{Arc, Mutex};

use rpc::RpcEmitter;

use serde_json::Value;

use xi_rpc::*;

/// XiClientProducer handle all the interactions with the core.
pub struct XiClientProducer {
    inner: Arc<Mutex<Box<Peer>>>,
}

impl XiClientProducer {
    pub fn new(xi_event_sender: Arc<Mutex<Box<Peer>>>) -> Self {
        XiClientProducer {
            inner: xi_event_sender,
        }
    }
}

impl RpcEmitter for XiClientProducer {
    fn send_notification(&self, method: &str, params: &Value) {
        let handle = self.inner.lock().unwrap();

        handle.send_rpc_notification(method, params);
    }

    fn send_request(&self, method: &str, params: &Value) -> Result<Value, Error> {
        let handle = self.inner.lock().unwrap();

        handle.send_rpc_request(method, params)
    }
}
