use serde_json::{self, Value};
use xi_rpc::*;

/// XiClientConsumer handle all the interactions with the core.
pub struct XiClientConsumer {}

impl Handler for XiClientConsumer {
    type Notification = RpcCall;
    type Request = RpcCall;

    fn handle_notification(&mut self, _ctx: &RpcCtx, rpc: Self::Notification) {
        match rpc.method.as_str() {
            "update" => println!("update notif {}", rpc.params),
            "scroll_to" => println!("scroll_to notif {}", rpc.params),
            "config_changed" => println!("config_changed notif {}", rpc.params),
            "available_plugins" => println!("available_plugins notif"),
            "available_themes" => println!("available_themes notif"),
            _ => println!(
                "notif from {} => {}",
                rpc.method,
                serde_json::to_string_pretty(&rpc.params).unwrap()
            ),
        }
    }

    fn handle_request(&mut self, _ctx: &RpcCtx, rpc: Self::Request) -> Result<Value, RemoteError> {
        match rpc.method {
            _ => println!(
                "request from {} => {}",
                rpc.method,
                serde_json::to_string_pretty(&rpc.params).unwrap()
            ),
        }

        Ok(json!({"foo": "bar"}))
    }
}
