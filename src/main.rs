#[macro_use]
extern crate serde_json;
extern crate xi_rpc;

mod rpc;

use std::thread;

use rpc::RpcEmitter;

fn main() {
    let mut client = rpc::start_xi_core();

    client.send_notification(
        "client_started",
        &json!({
            "config_dir": "~/.config/xi"
        }),
    );

    let res = client.send_request("new_view", &json!({}));
    let view_id = res.unwrap();

    println!("view: {}", view_id);
    client.send_notification(
        "edit",
        &json!({
            "view_id": view_id,
            "method": "insert",
            "params": {
                "chars": "foo"
            }
        }),
    );

    println!("Hello, world!");
    thread::sleep(::std::time::Duration::from_secs(40));
}
